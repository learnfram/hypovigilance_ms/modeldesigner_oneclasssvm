# src/app/validation.py

from flask_inputs import Inputs
from flask_inputs.validators import JsonSchema

input_schema = {
    'type': 'object',
    'properties': {
        'greetee': {
            'type': 'string',
        }
    },
    'required': ['greetee']
}


class RequestInputs(Inputs):
    json = [JsonSchema(schema=input_schema)]


def validate_request(request):
    inputs = RequestInputs(request)
    if inputs.validate():
        return None
    else:
        return inputs.errors
