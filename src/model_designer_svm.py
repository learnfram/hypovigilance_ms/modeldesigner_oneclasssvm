# src/app/main.py

from flask import Flask, request, jsonify
import numpy as np
from sklearn.svm import OneClassSVM

from src.app.invalid_usage import InvalidUsage
from src.app.validation import validate_request
from src.app.utils import csvToList, GetEntropy, Calcul_gamma


app = Flask(__name__)


@app.route('/')
def hello():
    user_agent = request.headers.get('User-Agent')
    return 'Welcome to hypovigilance detector ! I see you are using %s' % user_agent


@app.route('/model_designer_svm', methods=['POST'])
def model_designer_svm():
    errors = validate_request(request)
    if errors is not None:
        print(errors)
        raise InvalidUsage(errors)

    input_data = request.get_json()
    nu = input_data['nu'] if hasattr(input_data, 'nu') else 0.1
    kernel = input_data['kernel'] if hasattr(input_data, 'kernel') else "rbf"
    gamma = input_data['gamma'] if hasattr(input_data, 'gamma') else 0.1

    classifier = OneClassSVM(nu, kernel, gamma)
    return classifier


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
