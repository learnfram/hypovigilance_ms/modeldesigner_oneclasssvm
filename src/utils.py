# src/app/utils.py

import csv
import numpy as np
import pywt
from scipy.stats import entropy


def Calcul_gamma(X):
    # Jaakkola pour le calcul du gamma du one-class SVM
    n,m=X.shape
    gamma=0
    distance=np.zeros((n,n))
    for i in range(n):
        for j in range(n):
            d=0
            for k in range(m):
                d=d+(X[i,k] - X[j,k])**2
            distance[i,j]=np.sqrt(d)
    gamma=1/(2*((np.median(distance))**2))
    return gamma


def ent1(x):
    a=sum(x)
    E=[]
    for i in range(0,10):
        E.append(x[i]/a)
    return E


def Entropy_(labels):
    """ Computes entropy of 0-1 vector. """
    n_labels = len(labels)
    if n_labels <= 1:
        return 0
    counts = np.bincount(labels)
    probs = counts[np.nonzero(counts)] / n_labels
    n_classes = len(probs)
    if n_classes <= 1:
        return 0
    return - np.sum(probs * np.log(probs)) / np.log(n_classes)


def GetEntropy(signal):
    """
    Calculates the entropy of a signal
    Input: the signal
    Output: the Shannon enropy
    """
    hist, bin_edges = np.histogram(signal, density=False)
    hist = hist/len(signal)
    return entropy(hist, base=2)


def csvToList(fname):
    with open(fname, newline='') as f:
        reader = csv.reader(f)
        return list(reader)


def wrcoef(X, coef_type, coeffs, wavename, level):
    N = np.array(X).size
    a, ds = coeffs[0], list(reversed(coeffs[1:]))
    if coef_type =='a':
        return pywt.upcoef('a', a, wavename, level=level)[:N]
    elif coef_type == 'd':
        return pywt.upcoef('d', ds[level-1], wavename, level=level)[:N]
    else:
        raise ValueError("Invalid coefficient type: {}".format(coef_type))


def GetWaves(signal,Wtype='db5',level=5):
    """
    Calculates the different waves of an EEG signal
    Input: the signal , Wtype: wavelet type, level: decomposition level
    Output: dictionary with the different extracted waves
    """
    coeffs  = pywt.wavedec(signal,Wtype, level=level)
    Theta   = wrcoef(signal, 'a', coeffs, Wtype, 5)
    Delta   = wrcoef(signal, 'd', coeffs, Wtype, 5)
    Beta    = wrcoef(signal, 'd', coeffs, Wtype, 3)
    Alpha   = wrcoef(signal, 'd', coeffs, Wtype, 4)
    Gamma   = wrcoef(signal, 'd', coeffs, Wtype, 2)
    Waves   = {}
    Waves['alpha'] = Alpha
    Waves['beta']  = Beta
    Waves['gamma'] = Gamma
    Waves['theta'] = Theta
    Waves['delta'] = Delta
    return Waves


def GetEntropyWaves(Waves):
    Alpha = Waves['alpha']
    Beta = Waves['beta']
    Gamma = Waves['gamma']
    Theta = Waves['theta']
    Delta = Waves['delta']
    EntropyWaves = {}
    EntropyWaves['alpha'] = GetEntropy(Alpha)
    EntropyWaves['beta']  = GetEntropy(Beta)
    EntropyWaves['gamma'] = GetEntropy(Gamma)
    EntropyWaves['delta'] = GetEntropy(Delta)
    EntropyWaves['theta'] = GetEntropy(Theta)
    return EntropyWaves


def GetEnergyWaves(Waves):
    Alpha = Waves['alpha']
    Beta  = Waves['beta']
    Gamma = Waves['gamma']
    Theta = Waves['theta']
    Delta = Waves['delta']
    EnergyWaves = {}
    EnergyWaves['alpha'] = np.sum(Alpha**2)
    EnergyWaves['beta']  = np.sum(Beta**2)
    EnergyWaves['gamma'] = np.sum(Gamma**2)
    EnergyWaves['delta'] = np.sum(Delta**2)
    EnergyWaves['theta'] = np.sum(Theta**2)
    return EnergyWaves


def GetEnergyRatioWaves(Waves):
    EnergyRatioWaves = GetEnergyWaves(Waves)
    S =  sum(EnergyRatioWaves.values())
    EnergyRatioWaves['alpha']    = EnergyRatioWaves['alpha']  / S
    EnergyRatioWaves['beta']     = EnergyRatioWaves['beta']   / S
    EnergyRatioWaves['gamma']    = EnergyRatioWaves['gamma'] / S
    EnergyRatioWaves['delta']    = EnergyRatioWaves['delta'] / S
    EnergyRatioWaves['theta']    = EnergyRatioWaves['theta']  / S
    return EnergyRatioWaves


def GetWavesMin(Waves):
    WavesMin = {}
    WavesMin['alpha']    = np.min(Waves['alpha'])
    WavesMin['beta']     = np.min(Waves['beta'])
    WavesMin['gamma']    = np.min(Waves['gamma'])
    WavesMin['delta']    = np.min(Waves['delta'])
    WavesMin['theta']    = np.min(Waves['theta'])
    return WavesMin


def GetWavesMax(Waves):
    WavesMax = {}
    WavesMax['alpha']    = np.max(Waves['alpha'])
    WavesMax['beta']     = np.max(Waves['beta'])
    WavesMax['gamma']    = np.max(Waves['gamma'])
    WavesMax['delta']    = np.max(Waves['delta'])
    WavesMax['theta']    = np.max(Waves['theta'])
    return WavesMax


def GetWavesVar(Waves):
    WavesVar = {}
    WavesVar['alpha']    = np.var(Waves['alpha'])
    WavesVar['beta']     = np.var(Waves['beta'])
    WavesVar['gamma']    = np.var(Waves['gamma'])
    WavesVar['delta']    = np.var(Waves['delta'])
    WavesVar['theta']    = np.var(Waves['theta'])
    return WavesVar


def GetWavesMean(Waves):
    WavesMean = {}
    WavesMean['alpha']    = np.mean(Waves['alpha'])
    WavesMean['beta']     = np.mean(Waves['beta'])
    WavesMean['gamma']    = np.mean(Waves['gamma'])
    WavesMean['delta']    = np.mean(Waves['delta'])
    WavesMean['theta']    = np.mean(Waves['theta'])
    return WavesMean
